using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageResizeDigdin {
	public partial class Form1 : Form {
		public Form1() {
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e) {
			this.AllowDrop = true;
			this.DragEnter += Form1_DragEnter;
			this.DragDrop += Form1_DragDrop;
		}

		private void Form1_DragEnter(object sender, DragEventArgs e) {
			if (e.Data.GetDataPresent(DataFormats.FileDrop)) {
				e.Effect = DragDropEffects.Copy;
			}
			else {
				e.Effect = DragDropEffects.None;
			}
		}

		private void Form1_DragDrop(object sender, DragEventArgs e) {
			if (e.Data.GetDataPresent(DataFormats.FileDrop)) {
				string[] filePaths = (string[])(e.Data.GetData(DataFormats.FileDrop));

				foreach (string fileLoc in filePaths) {
					// Code to read the contents of the text file
					if (File.Exists(fileLoc)) {
						string fileExtension = Path.GetExtension(fileLoc).ToLower();
						if(fileExtension == ".png" || fileExtension == ".jpg") {
							if (checkBox1.Checked) {
								MessageBox.Show("converting " + fileLoc + " now");
							}
							
							ExecuteCommand(TranslateIntoResizeCommand(fileLoc));
						}
					}

				}
			}
		}

		/// <summary>
		/// returns closest number that is divisible by 4
		/// </summary>
		/// <param name="initialValue"></param>
		/// <returns></returns>
		int GetClosestToMulti4(int initialValue) {
			int restus = initialValue % 4;
			if(restus != 0) {
				if (restus >= 2) {
					return initialValue + (4 - restus);
				}
				else {
					return initialValue - restus;
				}
			}
			else {
				return initialValue;
			}
			
		}

		string TranslateIntoResizeCommand(string theImgLoc) {
			using (Image convertedImg = Image.FromFile(theImgLoc)) {
				string destPath = Application.StartupPath + "/resizedByDigdin";
				string targetResizeValue = GetClosestToMulti4(convertedImg.Width).ToString() + "x" + GetClosestToMulti4(convertedImg.Height).ToString();
				if (!Directory.Exists(destPath)) {
					Directory.CreateDirectory(destPath);
				}
				return "mogrify -path \"" + @destPath + "\" -resize " + @targetResizeValue + "^! \"" + @theImgLoc + "\"";
			}
        }

		static void ExecuteCommand(string command) {
			var processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
			processInfo.CreateNoWindow = false;
			processInfo.UseShellExecute = false;
			processInfo.RedirectStandardError = true;
			processInfo.RedirectStandardOutput = true;

			var process = Process.Start(processInfo);

			process.OutputDataReceived += (object sender, DataReceivedEventArgs e) =>
				Console.WriteLine("output>>" + e.Data);
			process.BeginOutputReadLine();

			process.ErrorDataReceived += (object sender, DataReceivedEventArgs e) =>
				Console.WriteLine("error>>" + e.Data);
			process.BeginErrorReadLine();

			process.WaitForExit();

			Console.WriteLine("ExitCode: {0}", process.ExitCode);
			process.Close();
		}

		private void checkBox1_CheckedChanged(object sender, EventArgs e) {
			//nada por enquanto, mas viria aqui
		}
	}
}
